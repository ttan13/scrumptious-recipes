from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from tags.models import Tag


# Create your views here.
class TagsListView(ListView):
    model = Tag
    template_name = "tags/list.html"
    paginate_by = 6


class TagsDetailView(DetailView):
    model = Tag
    template_name = "tags/details.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        return context


class TagsCreateView(LoginRequiredMixin, CreateView):
    model = Tag
    template_name = "tags/create.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("tags_list")


class TagsUpdateView(LoginRequiredMixin, UpdateView):
    model = Tag
    template_name = "tags/edit.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("tags_list")


class TagsDeleteView(LoginRequiredMixin, DeleteView):
    model = Tag
    template_name = "tags/delete.html"
    success_url = reverse_lazy("tags_list")
