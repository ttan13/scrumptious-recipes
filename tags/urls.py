from django.urls import path

from tags.views import (
    TagsListView,
    TagsDetailView,
    TagsCreateView,
    TagsUpdateView,
    TagsDeleteView,
)



urlpatterns = [
    path("", TagsListView.as_view(), name="tags_list"),
    path("<int:pk>/", TagsDetailView.as_view(), name="tag_detail"),
    path("new/", TagsCreateView.as_view(), name="tag_new"),
    path("<int:pk>/edit/", TagsUpdateView.as_view(), name="tag_edit"),
    path("<int:pk>/delete/", TagsDeleteView.as_view(), name="tag_delete"),
]
