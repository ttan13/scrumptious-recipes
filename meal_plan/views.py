from django.shortcuts import render
from django.urls import reverse_lazy
from meal_plan.models import MealPlan
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from recipes.models import Recipe


# Create your views here.


class MealPlanListView(ListView):
    model = MealPlan
    template_name = "meal_plan/list.html"
    paginate_by = 5


class MealPlanDetailView(DetailView):
    model = MealPlan
    template_name = "meal_plan/detail.html"
    context_object_name = "mealplan"


# def get_queryset(self):
#     return MealPlan.objects.filter(owner=self.request.user)


class MealPlanCreateView(CreateView):
    model = MealPlan
    template_name = "meal_plan/new.html"
    fields = ["name", "recipes"]
    success_url = reverse_lazy("meal_plan_new")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class MealPlanDeleteView(DeleteView):
    model = MealPlan
    template_name = "meal_plan/delete.html"
    success_url = reverse_lazy("meal_plan_list")


class MealPlanUpdateView(UpdateView):
    model = MealPlan
    template_name = "meal_plan/edit.html"
    fields = ["name", "date", "recipes"]
    sucess_url = reverse_lazy("meal_plan_list")
