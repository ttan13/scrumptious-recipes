from django.urls import include
from django.urls import path
from django.urls import reverse_lazy
from django.views.generic.base import RedirectView
from django.contrib.auth import views as auth_views

from meal_plan.views import (
    MealPlanCreateView,
    MealPlanListView,
    MealPlanDetailView,
    MealPlanUpdateView,
    MealPlanDeleteView,
)


urlpatterns = [
    # path("meal_plan/", include("meal_plan.urls")),
    path("meal_plan/", MealPlanListView.as_view(), name="meal_plan_list"),
    path(
        "meal_plan/create/", MealPlanCreateView.as_view(), name="meal_plan_new"
    ),
    path(
        "meal_plan/<int:pk>/",
        MealPlanDetailView.as_view(),
        name="meal_plan_detail",
    ),
    path(
        "meal_plan/<int:pk>/edit/",
        MealPlanUpdateView.as_view(),
        name="meal_plan_edit",
    ),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plan_delete",
    ),
    path(
        "", RedirectView.as_view(url=reverse_lazy("recipes_list")), name="home"
    ),
    path("accounts/login/", auth_views.LoginView.as_view(), name="login"),
    path("accounts/login/post/", auth_views.LoginView.as_view(), name="login"),
    path("accounts/logout/", auth_views.LogoutView.as_view(), name="logout"),
]
